/*jshint esversion: 6 */

class MeasurementsDiagram {
    initializeVariables()
    {
        this.diagrams = [];
        this.moveAxisX = 30;
        this.maxValue = 1.0;
    }

    calculateXAxisSize()
    {
        return this.parent.node().getBoundingClientRect().width - (this.moveAxisX * 2);
    }

    initializeXAxis()
    {


        this.zoom = d3.zoom()
            .on("zoom", this.zoomed.bind(this));

        window.addEventListener('resize', this.resize.bind(this));



        this.x_scale = d3.scaleLinear()
            .range([this.moveAxisX, this.calculateXAxisSize()])
            .domain([0, 0.001]);



        this.xAxisGenerator = d3.axisBottom()
            .scale(this.x_scale)
            .tickFormat(function(d, i)
                        {
                            return d;
                           // TODO
                           // var p = d3.precisionPrefix(3, d),
                           //     f = d3.formatPrefix("." + p + "s", d);
                           // return f(d) + "s";
                        }.bind(this));

        this.xAxis = this.svg.append("g")
            .attr("class", "x_axis")
            .attr("transform","translate(0,120)")
            .call(this.xAxisGenerator);

        this.svg.call(this.zoom)
            .on("dblclick.zoom", null)
            .append("g")
            .attr("transform","translate(0,0)");

    }

    constructor(container)
    {
        this.initializeVariables();
        var con = this.parent = d3.select(container);

        this.svg = con.append("svg")
            .attr("width", "100%")
            .attr("height", 500)
            .attr("class", "svg_canvas");

        this.initializeXAxis();
    }



    getTimeMultiplicator(time_unit)
    {
        var multiplicator = 0;
        switch (time_unit)
        {
            case 's':
            multiplicator = 1;
            break;

            case 'ms':
            multiplicator = Math.pow(10, -3);
            break;

            case 'us':
            multiplicator = Math.pow(10, -6);
            break;

            case 'ns':
            multiplicator = Math.pow(10, -9);
            break;

            case 'ps':
            multiplicator = Math.pow(10, -12);
            break;
        }
        return multiplicator;
    }


    generateDataArrayStampedLogic(measurements)
    {
        var time_multiplier = this.getTimeMultiplicator(measurements.resolution);
        var currentValue = measurements.init_state;
        var data = [[0, currentValue]];
        var currentTime = 0;

        for(let point in measurements.points)
        {
            if (measurements.diffStamps)
            {
                currentTime += measurements.points[point] * time_multiplier;
            }
            else
            {
                currentTime = measurements.points[point] * time_multiplier;
            }

            data.push([currentTime, currentValue]);

            if(currentValue)
            {
                currentValue = 0;
            }
            else
            {
                currentValue = 1;
            }

            data.push([currentTime, currentValue]);
        }
        return data;
    }


    generateDataArray(measurements)
    {
        var data = [];
        var type = measurements.timing_type + "_" + measurements.data_type;
        switch(type)
        {
            case 'stamped_logic':
            data = this.generateDataArrayStampedLogic(measurements);
            break;
        }

        var l = data[data.length - 1];
        data.push([l[0] +  this.getTimeMultiplicator(measurements.resolution), l[1]]);

        return data;
    }

    addMeasurements(measurements)
    {
        var lineDiagram = {};
        lineDiagram.data = this.generateDataArray(measurements);
        lineDiagram.x_scale = this.x_scale;
        var x = this.x_scale.bind(this);


        var diag = this.diagrams.length;

        lineDiagram.y_scale = d3.scaleLinear()
            .range([100 * (diag + 1), (100 * diag) + 10])
            .domain([-0.1, 1.1]);

        lineDiagram.yAxisGenerator = d3.axisLeft()
            .scale(lineDiagram.y_scale);

        if (measurements.data_type == 'logic')
        {
            lineDiagram.yAxisGenerator = lineDiagram.yAxisGenerator.ticks(1);
        }

        lineDiagram.lineGenerator = d3.line()
            .x(function(d) { return x(d[0]); })
            .y(function(d) { return lineDiagram.y_scale(d[1]); });

        lineDiagram.yAxis = this.svg.append("g")
            .attr("class", "y_axis")
            .attr("transform","translate("+ this.moveAxisX +",0)")
            .call(lineDiagram.yAxisGenerator);

        lineDiagram.line = this.svg.append("path")
            .data([lineDiagram.data])
//            .attr("transform","translate("+ this.moveAxisX +",0)")
            .attr("d", lineDiagram.lineGenerator)
            .attr("class","population_line")
            .attr("vector-effect", "non-scaling-stroke");


        this.diagrams.push(lineDiagram);
    }

    getMaxTime()
    {
        for (let dia in this.diagrams)
        {
            //diagram_last = this.diagrams[dia].data;
        }
    }


    resize()
    {
        var newRange = this.calculateXAxisSize();
        var newDomain = this.x_scale.invert(newRange);
        this.x_scale
             .range([this.moveAxisX, newRange])
             .domain([0, newDomain]);
        this.xAxis.call(this.xAxisGenerator.scale(this.x_scale));
    }

    zoomed()
    {
        if (d3.event.transform.x > 0)
        {
            d3.event.transform.x = 0;
        }

        var translate_x = d3.event.transform.x;
        var transform_k = d3.event.transform.k;

        for(let l in this.diagrams)
        {
            this.diagrams[l].line.attr("transform",
                                       "translate(" + translate_x  + ",0)scale(" + transform_k + ",1)");
        }
        this.xAxis.call(this.xAxisGenerator.scale(d3.event.transform.rescaleX(this.x_scale)));
    }
}


var data = {
    "title": "channel 1",
    "data_type": "logic",
    "timing_type": "stamped",
    "resolution": "us",
    "init_state": 0,
    "diffStamps": false,
    "points": [
        100,
        140,
        201,
        290,
        400,
        450,
        600,
        900,
        950,
        960,
        970,
        990,
        1000,
    ]
};

var data2 = {
    "title": "channel 2",
    "data_type": "logic",
    "timing_type": "stamped",
    "resolution": "us",
    "init_state": 1,
    "diffStamps": true,
    "points": [
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
    ]
};

measure = new MeasurementsDiagram("#diagrams");
measure.addMeasurements(data);
measure.addMeasurements(data2);
//measure.addMeasurements(data);
//measure.addMeasurements(data2);

